#ifndef lst_h
#define lst_h

#include "lst_elm.h"

struct lst_t {
    struct lst_elm_t *head;
    struct lst_elm_t *tail;
    int numelm;
};

struct lst_t * new_lst(void);
void del_lst(struct lst_t **ptrL);
int empty_lst(const struct lst_t *L);
void cons(struct lst_t *L, int v);
void print_lst(struct lst_t *L);
void insert_ordered(struct lst_t *L, const int value);

#endif
