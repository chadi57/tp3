#include "lst.h"

void insert_after (struct lst_t *L, const int value, struct lst_elm_t *place);

struct lst_t * new_lst(void) {
    /**
     * @note : calloc fonctionne de manière identique à malloc
     * et de surcroît met à NULL(0) tous les octets alloués
     **/
    struct lst_t *L = (struct lst_t*)calloc(1,sizeof(struct lst_t));
    assert(L);
    L->head = NULL;
    L->tail = NULL;
    L->numelm = 0;
    return L;
}

void del_lst(struct lst_t **prtL) {
    struct lst_elm_t *aSupprimer = (*prtL)->head;
    (*prtL)->head = aSupprimer->suc;
    free(aSupprimer);
    (*prtL)->numelm--;
    if ((*prtL)->numelm-- == 0) {
        (*prtL)->tail = NULL;
    }
}

int empty_lst(const struct lst_t *L) {
    assert(L);// L doit exister !
    return L->numelm == 0;
}

void cons(struct lst_t *L, int v) {
    struct lst_elm_t *nouvElement = (struct lst_elm_t *)calloc(1,sizeof(struct lst_elm_t));
    nouvElement->x = v;
    nouvElement->suc = L->head;
    L->head = nouvElement;
    L->numelm++;
    if (L->numelm == 1) {
        L->tail = L->head;
    }
}

void print_lst(struct lst_t *L) {
    printf( "[ " );
    for( struct lst_elm_t * E = L->head; E; E = E->suc) {
        printf( "%d ", E->x );
    }
    printf("]\n\n");
}

void insert_ordered(struct lst_t *L, const int value) {
    if (L->head == NULL)
        cons(L, value);
    else {
        struct lst_elm_t *recherche = L->head;
        while ((recherche != NULL) && (value > recherche->suc->x)) {
            recherche = recherche->suc;
        }
        struct lst_elm_t *nouvElement = (struct lst_elm_t *)calloc(1,sizeof(struct lst_elm_t));
        nouvElement->x = value;
        nouvElement->suc = recherche->suc;
        recherche->suc = nouvElement;
    }
}

void insert_after (struct lst_t *L, const int value, struct lst_elm_t *place) {
    if (place == NULL)
        cons(L, value);
    else {
        struct lst_elm_t *nouvElement = (struct lst_elm_t *)calloc(1,sizeof(struct lst_elm_t));
        nouvElement->x = value;
        nouvElement->suc = place->suc;
        place->suc = nouvElement;
    }
 }
