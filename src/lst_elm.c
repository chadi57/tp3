#include "lst_elm.h"

struct lst_elm_t *new_lst_elm(int value) {
    struct lst_elm_t *nouvElement = (struct lst_elm_t*)calloc(1, sizeof(struct lst_elm_t));
    nouvElement->x = value;
    return  nouvElement;
}

void del_lst_elm_t(struct lst_elm_t **ptrE) {
    free(*ptrE);
}

int getX(struct lst_elm_t *E) {
    return E->x;
}

struct lst_elm_t *getSuc(struct lst_elm_t * E) {
    return E->suc;
}

void setX(struct lst_elm_t * E, int v) {
    E->x = v;
}

void setSuc(struct lst_elm_t *E, struct lst_elm_t *S) {
    struct lst_elm_t *aSupprimer = E->suc;
    E->suc = S;
    S->suc = aSupprimer->suc;
    free(aSupprimer);
}
